from django.apps import AppConfig


class WelcomeConfig(AppConfig):
    name = 'fwa1.apps.welcome'
